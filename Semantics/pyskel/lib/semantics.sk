(* Import Custom StdLib *)
#include "list.sk"
#include "maybe.sk"
#include "map.sk"
#include "heap.sk"
#include "bool.sk"

(** For debugging purposes, the specification of debug may perform some side effects *)
val debug : () -> ()


(*** 1. Python AST ***)
(* A python program is a list a statements (body).*)
(* It is the input type of the eval_stmts functions, wich is the main evaluation function *)
(* Python AST can be found here https://github.com/grievejia/pyre-ast/blob/trunk/lib/concrete.ml *)

(** Type of identifiers *)
type id

type python_int
type python_string
type binop = | Add

(* Python statement *)
type stmt =
  | SExpr expr                        (* expr *)
  | SAssign (id, expr)                (* id = expr *)
  | SAttrAssign (expr, id, expr)      (* expr.id = expr *)
  | SDel id                           (* del id *)
  | SNonlocal id                      (* nonlocal id *)
  | SGlobal id                        (* global id *)

  | SPass                             (* pass *)
  | SReturn expr                      (* return expr *)
  | SRaise expr                       (* raise expr *)
  | STry (body, exn_handler)          (* try: body with exn_handler *)

  | SFunctionDef (id, list<id>, body) (* name, args, body *)
  | SClassDef (id, list<expr>, list<(id, expr)>, body) (* name, args, kwargs, body *)

(** Type of Python programs *)
type body = list<stmt>

(** The matched exception the code to execute *)
type exn_handler = (expr, body)

type constant =
  | CNone
  | CBool bool
  | CInt python_int
  | CString python_string

type expr =
  | Constant constant
  | Name id
  | Call (expr, list<expr>)
  | Attribute (expr, id)
  | Addr addr
  | BinOp (binop, expr, expr)
    (* Used for internal functions *)
  | InternalToken internal

type internal = | IntAdd


(*** 2. Builtin Declarations ***)

(* Some strings needed by the interpreter *)
(* Builtin classes *)
val type_id : id
val function_id : id
val method_id : id
val list_id : id
val tuple_id : id
val notImplementedType_id : id
val noneType_id : id
val str_id : id
val int_id : id
val bool_id : id

(* Exceptions *)
val _BaseException : id
val _Exception : id
val _NameError : id
val _AttributeError : id
val _UnboundLocalError : id
val _TypeError : id

(* Special methods *)
val __init__ : id

(* Add two integers *)
val internal_int_add : (python_int, python_int) -> python_int

(** [op Add] is ["__add__"] *)
val op : binop -> id


(*** 3. Interpreter State ***)
(** 3.1 Builtins **)
type builtins =
  ( true: addr
  , false: addr
  , none: addr
  , notImplemented: addr
  )

val todo_error : addr

(** 3.2 Heap **)

(* The heap is a mapping from [addr] to pairs [obj_n, obj_s] *)
(* [obj_n] and [obj_s] are runtime representation of python values *)
(* See: https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.65 *)

(** Python object (nominal part) *)
type obj_n =
  | Int python_int
  | Bool bool
  | String python_string
  | None
  | NotImplemented
  | List list<addr>
  | Tuple list<addr>
  (* Fun (name, args, locals, body, scope) *)
  | Fun (id, list<id>, list<id>, body, maybe<scope_info>)
  | Method (addr, addr)
  | Class (id, list<addr>, id, map<addr>)
  | Inst addr
  | Internal internal

(** Structural part of an object *)
type obj_s =
  | Locked
  | Fields map<addr>

(** Heap addresses *)
type addr = heap_addr<(obj_n, obj_s)>
type python_heap = heap<(obj_n, obj_s)>

(* Specialize general functions for Python heap *)
val python_heap_alloc : (python_heap, (obj_n, obj_s)) -> (addr, python_heap)
  = heap_alloc<(obj_n, obj_s)>
val python_heap_read : (python_heap, addr) -> ((obj_n, obj_s), python_heap)
  = heap_read<(obj_n, obj_s)>
val python_heap_write : (python_heap, addr, (obj_n, obj_s)) -> ((), python_heap)
  = heap_write<(obj_n, obj_s)>

val addr_eq ((left: addr), (right: addr)) : bool =
  addr_equal<(obj_n, obj_s)> (left, right)

(** 3.3 Scopes *)
(** Gobal scope is a mapping from identifiers to addresses *)
type global_scope = map<addr>
val empty_scope : map<addr> = map_empty<addr>

type partial_addr = | LocalUndef | Value addr

(* Types for scope trees *)
type scope_info = (scope_id, map<var_scope>)
type partial_scope = (pscope_up: maybe<scope_info>, pmapping: map<partial_addr>)

type scope_id = heap_addr<partial_scope>
type scope_heap = heap<partial_scope>

(* Specialize general functions for scope heap *)
val scope_heap_alloc : (scope_heap, partial_scope) -> (scope_id, scope_heap)
  = heap_alloc<partial_scope>
val scope_heap_read : (scope_heap, scope_id) -> (partial_scope, scope_heap)
  = heap_read<partial_scope>
val scope_heap_write : (scope_heap, scope_id, partial_scope) -> ((), scope_heap)
  = heap_write<partial_scope>

(** Return the scope_up of any scope *)
val scope_up (scope: partial_scope) : maybe<scope_info> =
  scope.pscope_up

(** 3.4 Environment **)
type var_scope = | Local | NonLocal | Global

type env =
  | Globl
    (* var_scope and current scope *)
  | InFun scope_info
    (* var_scope and scope_up ! *)
  | InClass (map<var_scope>, maybe<scope_info>)

(** 3.5 Control-Flow Status **)
type flag =
  | Ret addr (* return *)
  | Brk      (* break *)
  | Cont     (* continue *)
  | Exn addr (* raise *)


(*** 4. Interpreter Monad ***)
(** 4.1 Types *)
type r = (local_env: env, builtins: builtins)
type s = ( global_scope: global_scope
         , scope_heap: scope_heap (* functions (shared) *)
         , scope_stack: list<map<addr>> (* classes (not shared) *)
         , heap: python_heap
         )
type exn<a> =
  | Cur a (* A value of type a *)
  | Flag flag (* A special state *)

(** Evaluation monad :: Reader r (State s (Except flag a)) *)
type m<a> = (r, s) -> (exn<a>, s)

(** Monadic return *)
val return<a> (v : a) : m<a> =
  \(_, s):(r, s) -> (Cur<a> v, s)

(** Monadic bind *)
val bind<a, b> ((o: m<a>), (f: a -> m<b>)) : m<b> =
  \(r, s):(r, s) ->
    let (a, s') = o (r, s) in
    match a with
    | Cur a ->
      let fa = f a in
      fa (r, s')
    | Flag f ->
      (Flag<b> f, s')
    end

binder @ = bind


(** 4.2 General Monadic Functions *)
val mapM<a, b> ((f: a -> m<b>), (l: list<a>)) : m<list<b>> =
  match l with
  | Nil -> return<list<b>> Nil<b>
  | Cons(h, t) ->
    let mh =@ f h in
    let mt =@ mapM<a, b> (f, t) in
    return<list<b>> (Cons<b>(mh, mt))
  end

val filterM<a> ((f: a -> m<bool>), (l: list<a>)) : m<list<a>> =
  match l with
  | Nil -> return<list<a>> Nil<a>
  | Cons(h, t) ->
    let b =@ f h in
    let t' =@ filterM<a> (f, t) in
    match b with
    | True -> let r = Cons<a>(h, t') in return<list<a>> r
    | False -> return<list<a>> t'
    end
  end

val zipWithM<a, b, c> ((f: (a, b) -> m<c>), (la: list<a>), (lb: list<b>)) : m<list<c>> =
  match (la, lb) with
  | (Nil, _) -> return<list<c>> Nil<c>
  | (_, Nil) -> return<list<c>> Nil<c>
  | (Cons(ha, ta), Cons(hb, tb)) ->
    let mh =@ f (ha, hb) in
    let mt =@ zipWithM<a, b, c> (f, ta, tb) in
    return<list<c>> (Cons<c>(mh, mt))
  end

val ask : m<r> =
  \(r, s):(r, s) -> (Cur<r> r, s)

val local<a> ((f: r -> r), (m: m<a>)) : m<a> =
  \(r, s):(r, s) ->
  let r' = f r in
  m (r', s)

val get : m<s> =
  \(_, s):(r, s) -> (Cur<s> s, s)

val gets<a> (f : s -> a) : m<a> =
  \(_, s):(r, s) -> let a = f s in (Cur<a> a, s)

val put (s: s) : m<()> =
  \(_, _):(r, s) -> (Cur<()> (), s)

val modify (f: s -> s) : m<()> =
  \(_, s):(r, s) -> let s' = f s in (Cur<()> (), s')

val set_flag<a> (f: flag) : m<a> =
  \(_, s):(r, s) -> (Flag<a> f, s)

val catch_flag<a> ((m: m<a>), (f: flag -> m<a>)) : m<a> =
  let s =@ get in
  let r =@ ask in
  let (res, s) = m (r, s) in
  put s ;@
  match res with
  | Cur x -> return<a> x
  | Flag flg -> f flg
  end

binder / = catch_flag

(** 4.3 State-Dependant Monadic Function *)
val ret (v : addr) : m<addr> = return<addr> v

val alloc (v:(obj_n, obj_s)) : m<addr> =
  let s =@ get in
  let (a, h') = python_heap_alloc (s.heap, v) in
  let s' = s <- (heap = h') in
  put s' ;@
  ret a

val read (a:addr) : m<(obj_n, obj_s)> =
  let s =@ get in
  let (v, h') = python_heap_read (s.heap, a) in
  let s' = s <- (heap = h') in
  put s' ;@
  return<(obj_n, obj_s)> v

val write ((a:addr), (v:(obj_n, obj_s))) : m<()> =
  let s =@ get in
  let (_, h') = python_heap_write (s.heap, a, v) in
  let s' = s <- (heap = h') in
  put s'

val create_total_scope_here (mapping: map<addr>): m<map<addr>> =
  return<map<addr>> mapping

val push_fresh_total_scope (():()): m<()> =
  modify (\s:s -> s <- (scope_stack = Cons<map<addr>> (map_empty<addr>, s.scope_stack)))

val pop_total_scope (():()): m<map<addr>> =
  let s =@ get in
  let (Cons (m, ms)) = s.scope_stack in
  put (s <- (scope_stack = ms)) ;@
  return<map<addr>> m

val get_total_scope (():()): m<map<addr>> =
  let s =@ get in
  let (Cons (m, _)) = s.scope_stack in
  return<map<addr>> m

val replace_top_stack (map: map<addr>) : m<()> =
  let s =@ get in
  let (Cons (_, tail)) = s.scope_stack in
  modify (\s:s -> s <- (scope_stack = Cons<map<addr>> (map, tail)))

val alloc_scope (v:partial_scope) : m<scope_id> =
  let s =@ get in
  let (a, sh') = scope_heap_alloc (s.scope_heap, v) in
  let s' = s <- (scope_heap = sh') in
  put s' ;@
  return<scope_id> a

val read_scope (a:scope_id) : m<partial_scope> =
  let s =@ get in
  let (v, sh') = scope_heap_read (s.scope_heap, a) in
  let s' = s <- (scope_heap = sh') in
  put s' ;@
  return<partial_scope> v

val write_scope ((a:scope_id), (v:partial_scope)) : m<()> =
  let s =@ get in
  let (_, sh') = scope_heap_write (s.scope_heap, a, v) in
  let s' = s <- (scope_heap = sh') in
  put s'

val delete_env (id: id) : m<()> =
  let s =@ get in
  let e' = map_del<addr> (id, s.global_scope) in
  let s' = s <- (global_scope = e') in
  put s'

val get_scope_up (():()) : m<maybe<scope_info>> =
  let r =@ ask in
  let scope_up = match r.local_env with
  | Globl -> Nothing<scope_info>
  | InFun sinfo -> Just<scope_info> sinfo
  | InClass(_, sinfo) -> sinfo
  end in
  return<maybe<scope_info>> scope_up

(*** 5. Read and Write in Scopes ***)
(** 5.1 Read **)
val find_global (id: id) : m<addr> =
  let s =@ get in
  let a = map_lookup<addr> (id, s.global_scope) in
  match a with
  | Just x -> ret x
  | Nothing -> raise_exn<addr> _NameError
  end

val find_local ((id: id), (sid: scope_id)) : m<maybe<partial_addr>> =
  let scope =@ read_scope sid in
  let res = map_lookup<partial_addr> (id, scope.pmapping) in
  return<maybe<partial_addr>> res

val find_nonlocal ((id: id), (sinfo: scope_info)) : m<addr> =
  let new_env = \r:r -> r <- (local_env = InFun sinfo) in
  let read_env_id = read_env id in
  local<addr> (new_env, read_env_id)

val read_env (id:id) : m<addr> =
  let r =@ ask in
  match r.local_env with
  | Globl -> (* We are in the global env, we just look for the variable in the global map *)
    find_global id
  | InFun (scope_id, e) -> (* We are evaluating a function, we refer to the local environment *)
    let scope_var = map_lookup<var_scope> (id, e) in
    match scope_var with
    | Nothing ->
        let scope =@ read_scope scope_id in
        let m_scope_up = scope.pscope_up in
        match m_scope_up with
        | Nothing -> find_global id
        | Just scope_up -> find_nonlocal (id, scope_up)
        end
    (* Look in the right scope *)
    | Just Local ->
        let (Just a) =@ find_local (id, scope_id) in
        match a with
        | Value a -> ret a
        (* LocalUndef *)
        | LocalUndef -> raise_exn<addr> _UnboundLocalError
        end
    | Just NonLocal ->
        let scope =@ read_scope scope_id in
        let (Just scope_up) = scope.pscope_up in
        find_nonlocal (id, scope_up)
    | Just Global -> find_global id
    end
  | InClass (e, m_scope_info) ->
    (* We are evaluating a class declaration, we refer to the local
       environment *)
    let scope_var = map_lookup<var_scope> (id, e) in
    match scope_var with
    | Nothing | Just Local -> (* Var may be local *)
      let map =@ get_total_scope () in
      let loc = map_lookup<addr> (id, map) in
      match loc with
      | Just addr -> ret addr
      | Nothing ->
        match m_scope_info with
        | Nothing -> find_global id
        | Just scope_info -> find_nonlocal (id, scope_info)
        end
      end
    | Just NonLocal ->
        let (Just scope_info) = m_scope_info in
        find_nonlocal (id, scope_info)
    | Just Global -> find_global id
    end
  end

(** 5.2 Write **)
val write_local ((id: id), (a: addr), ((sid, _): scope_info)) : m<()> =
  let scope =@ read_scope sid in
  let mem = map_elem<partial_addr> (id, scope.pmapping) in
  match mem with
  | False -> set_flag<()> (Exn todo_error)
  | True ->
      let m' = map_insert<partial_addr> (id, (Value a), scope.pmapping) in
      let s' = scope <- (pmapping = m') in
      write_scope (sid, s')
  end

val write_nonlocal ((id: id), (a: addr), (sinfo: scope_info)) : m<()> =
  let new_env = \r:r -> r <- (local_env = InFun sinfo) in
  let write_env_id_a = write_env (id, a) in
  local<()> (new_env, write_env_id_a)

val write_env ((id: id), (a: addr)) : m<()> =
  let s =@ get in
  let r =@ ask in
  match r.local_env with
  | Globl ->
    let gs' = map_insert<addr> (id, a, s.global_scope) in
    let s' = s <- (global_scope = gs') in
    put s'
  | InFun scope_info ->
    let (_, e) = scope_info in
    let scope_var = map_lookup<var_scope> (id, e) in
    match scope_var with
      (* Local scope *)
    | Just Local ->
      write_local (id, a, scope_info)
    | Nothing | Just NonLocal ->
      let (scope_id, _) = scope_info in
      let scope =@ read_scope scope_id in
      let (Just scope_up) = scope.pscope_up in
      write_nonlocal (id, a, scope_up)
      (* In global scope, modify the value *)
    | Just Global ->
      let gs' = map_insert<addr> (id, a, s.global_scope) in
      let s' = s <- (global_scope = gs') in
      put s'
    end
  | InClass (e, m_scope_info) ->
    let scope_var = map_lookup<var_scope> (id, e) in
    match scope_var with
      (* Not in scope => add to current scope (not like InFun) *)
    | Nothing | Just Local ->
      let map =@ get_total_scope () in
      let map = map_insert<addr> (id, a, map) in
      replace_top_stack map
    | Just NonLocal ->
      let (Just scope_info) = m_scope_info in
      write_nonlocal (id, a, scope_info)
      (* In global scope, modify the value *)
    | Just Global ->
      let gs' = map_insert<addr> (id, a, s.global_scope) in
      let s' = s <- (global_scope = gs') in
      put s'
    end
  end


(*** 6. The Interpreter ***)
(** 6.1 Inital State **)
val initialized_heap (() : ()) : (r, python_heap) =
  let h = initialHeap<(obj_n, obj_s)> in
  let (true , h) = python_heap_alloc (h, (Bool True , Locked)) in
  let (false, h) = python_heap_alloc (h, (Bool False, Locked)) in
  let (none , h) = python_heap_alloc (h, (None      , Locked)) in
  let (notImplemented, h) = python_heap_alloc (h, (NotImplemented, Locked)) in
  (( local_env = Globl
   , builtins = ( true = true
                , false = false
                , none = none
                , notImplemented = notImplemented
                )
   ), h)


(** 6.2 Internal Functions **)
val call_internal ((internal : internal), (args : list<addr>)) : m<addr> =
  match internal with
  | IntAdd ->
     match args with
     | Cons (op1, Cons (op2, Nil)) ->
       let (objn1, _) =@ read op1 in
       let (objn2, _) =@ read op2 in
       match (objn1, objn2) with
       | (Int left, Int right) ->
          let result = internal_int_add (left, right) in
          alloc (Int result, Locked)
       | _ ->
          let r =@ ask in
          ret r.builtins.notImplemented
       end
     | _ ->
        raise_exn<addr> _TypeError
     end
  end

(** 6.3 Functions **)
val locals (ss: body) : list<id> =
  match ss with
  | Nil -> Nil<id>
  | Cons(SAssign(x, _), ss') ->
    let tail = locals ss' in
    Cons<id>(x, tail)
  | Cons(SFunctionDef(name, _, _), ss') ->
    let tail = locals ss' in
    Cons<id>(name, tail)
  | Cons(SClassDef(name, _, _, _), ss') ->
    let tail = locals ss' in
    Cons<id>(name, tail)
  | Cons(_, ss') -> locals ss'
  end

val nonlocals (ss: body) : list<id> =
  match ss with
  | Nil -> Nil<id>
  | Cons(SNonlocal x, ss') ->
    let tail = nonlocals ss' in
    Cons<id>(x, tail)
  | Cons(_, ss') -> nonlocals ss'
  end

val globals (ss: body) : list<id> =
  match ss with
  | Nil -> Nil<id>
  | Cons(SGlobal x, ss') ->
    let tail = globals ss' in
    Cons<id>(x, tail)
  | Cons(_, ss') -> nonlocals ss'
  end

val declarations' (body: body): m<map<addr>> =
  let non_locals = nonlocals body in
  let global_vars = globals body in
  let fun_env =
    let is_nonlocal = \((m, s):(map<var_scope>, id)) -> map_insert<var_scope> (s, NonLocal, m) in
    let is_global = \((m, s):(map<var_scope>, id)) -> map_insert<var_scope> (s, Global, m) in
    let m = foldl<id, map<var_scope>> (is_nonlocal, map_empty<var_scope>, non_locals) in
    foldl<id, map<var_scope>> (is_global, m, global_vars) in

  let scope_up =@ get_scope_up () in

  let in_scope = \r:r ->
    let local_env' = InClass (fun_env, scope_up) in
    r <- (local_env = local_env')
  in

  push_fresh_total_scope ();@
  let eval_body = eval_stmts body ;@ return<()> () in
  local<()> (in_scope, eval_body) ;@
  pop_total_scope ()

val eval_fun_call ((at_args: list<addr>), (a: list<id>), (loc: list<id>), (body: body), (scope_up: maybe<scope_info>)): m<addr> =
  let r =@ ask in
  (* Eval and set the arguments *)
  let args_with_values = zip<id, addr> (a, at_args) in

  (* Set the local variables to LocalUndef *)
  (* Can s.scope change between here and the beginning of the function? *)
  let locals_undef =
    let f = \((m, s):(map<partial_addr>, id)) -> map_insert<partial_addr> (s, LocalUndef, m) in
    foldl<id, map<partial_addr>> (f , map_empty<partial_addr> , loc) in
  let locals_with_args =
    let f = \((m, (s, a)):(map<partial_addr>, (id, addr))) -> map_insert<partial_addr> (s, Value a, m) in
    foldl<(id, addr), map<partial_addr>> (f , locals_undef , args_with_values) in
  let inner_scope = (pscope_up = scope_up, pmapping = locals_with_args) in
  let inner_scope_id =@ alloc_scope inner_scope in

  let non_locals = nonlocals body in
  let global_vars = globals body in
  let all_locals = append<id> (loc, a) in
  let fun_env =
    let is_local = \((m, s):(map<var_scope>, id)) -> map_insert<var_scope> (s, Local, m) in
    let is_nonlocal = \((m, s):(map<var_scope>, id)) -> map_insert<var_scope> (s, NonLocal, m) in
    let is_global = \((m, s):(map<var_scope>, id)) -> map_insert<var_scope> (s, Global, m) in
    let m = foldl<id, map<var_scope>> (is_local , map_empty<var_scope> , all_locals) in
    let m' = foldl<id, map<var_scope>> (is_nonlocal, m, non_locals) in
    foldl<id, map<var_scope>> (is_global, m', global_vars) in

  let set_locals = \r:r ->
    let local_env' = InFun (inner_scope_id, fun_env) in
    r <- (local_env = local_env')
  in

  let body_evaluation =
    eval_stmts body ;@
    return<addr> r.builtins.none
  in

  let local_body_eval = local<addr> (set_locals, body_evaluation) in

  let flag =/ local_body_eval in
  match flag with
  | Ret a -> return<addr> a
  | f -> set_flag<addr> f
  end

val raise<a> (at_exn : addr) : m<a> =
  let baseException =@ read_env _BaseException in
  let is_exn =@ is_subclass_of (at_exn, baseException) in
  match is_exn with
  | True ->
    let at =@ eval_class_call (Nil<addr>, at_exn, map_empty<addr>) in
    set_flag<a> (Exn at)
  | False -> set_flag<a> (Exn at_exn)
  end

(** 6.4 Classes *)
val typeof (a: addr): m<addr> =
  let (objn, _) =@ read a in
  match objn with
  | Inst c         -> ret c
  | Class _        -> read_env type_id
  | Fun _          -> read_env function_id
  | Method _       -> read_env method_id
  | List _         -> read_env list_id
  | Tuple _        -> read_env tuple_id
  | NotImplemented -> read_env notImplementedType_id
  | None           -> read_env noneType_id
  | String _       -> read_env str_id
  | Int _          -> read_env int_id
  | Bool _         -> read_env bool_id
  | Internal _     -> read_env function_id
  end

val eval_class_call ((at_args: list<addr>), (class: addr), (dict: map<addr>)): m<addr> =
  let inst = Inst class in
  let init_method = map_lookup<addr> (__init__, dict) in
  let self =@ alloc (inst, Fields map_empty<addr>) in

  match init_method with
  | Nothing -> return<()> ()
  | Just m ->
    let (objn, _) =@ read m in
    match objn with
    | Fun(_, a, loc, body, scope_up) ->
       let new_args = Cons<addr> (self, at_args) in
       eval_fun_call (new_args, a, loc, body, scope_up);@
       return<()> ()
    | _ -> raise_exn<()> _TypeError
    end
  end ;@
  ret self


val is_subclass_of ((at_child: addr), (at_super: addr)) : m<bool> =
  let is_eq = addr_eq (at_child, at_super) in
  match is_eq with
  | True -> return<bool> True
  | False ->
    let (child, _) =@ read at_child in
    match child with
    | Class (_, c_supers, _, _) ->
      let is_subclass_of_super = \c:addr -> is_subclass_of (c, at_super) in
      let bs =@ mapM<addr, bool> (is_subclass_of_super, c_supers) in
      let res = foldl<bool, bool> (or', False, bs) in
      return<bool> res
    | _ -> raise_exn<bool> _TypeError
    end
  end

val is_instance_of ((at_object: addr), (at_class: addr)) : m<bool> =
  let (object, _) =@ read at_object in
  match object with
  | Inst c -> is_subclass_of (c, at_class)
  | _ -> return<bool> False
  end


val has_attribute ((at_e: addr), (attr: id)) : m<bool> =
  let (objn, objs) =@ read at_e in
  match objn with
  | Class (_, supers, _, dict) ->
    let get_builtin = map_lookup<addr> (attr, dict) in
    match get_builtin with
    | Nothing ->
      match objs with
      | Locked -> return<bool> False
      | Fields map ->
        let get_field = map_lookup<addr> (attr, map) in
        match get_field with
        | Nothing -> return<bool> False
        | Just a -> return<bool> True
        end
      end
    | Just a -> return<bool> True
    end
  | Inst c ->
    match objs with
    | Locked -> return<bool> False
    | Fields map ->
      let get_field = map_lookup<addr> (attr, map) in
      match get_field with
      | Nothing -> has_attribute (c, attr)
      | Just a -> return<bool> True
      end
    end
  | _ -> return<bool> False
end

val get_attribute ((at_e: addr), (attr: id)) : m<addr> =
  let (objn, objs) =@ read at_e in
  match objn with
  | Class (_, supers, _, dict) ->
    (* Look in objs first *)
    match objs with
    | Locked -> raise_exn<addr> _AttributeError
    | Fields map ->
      let get_field = map_lookup<addr> (attr, map) in
      match get_field with
      | Just a -> ret a
      | Nothing -> 
        (* We inlined get_builtin_field here *)
        let get_builtin = map_lookup<addr> (attr, dict) in
        match get_builtin with
        | Just a -> ret a
        | Nothing ->
          (* look in super classes *)
          let has_attribute_attr = \super:addr -> has_attribute (super, attr) in
          let supers_with_attribute =@ filterM<addr> (has_attribute_attr, supers) in
          match supers_with_attribute with
          | Cons(h, _) -> get_attribute (h, attr)
          | Nil -> raise_exn<addr> _AttributeError
          end
        end
      end
    end
  | Inst c ->
    match objs with
    | Locked -> raise_exn<addr> _AttributeError
    | Fields map ->
      let get_field = map_lookup<addr> (attr, map) in
      match get_field with
      | Nothing ->
          (* Look for methods *)
          (* We inlined function.__get__ *)
          let attr =@ get_attribute (c, attr) in
		      let (objn, _) =@ read attr in
		      match objn with
		      | Fun _ ->
                let meth = Method (at_e, attr) in
                let at_meth =@ alloc (meth, Locked) in
                ret at_meth
		      | _ -> ret attr
		      end
      | Just a -> ret a
      end
    end
  | Int _ ->
    let class_int =@ read_env int_id in
    let attr =@ get_attribute (class_int, attr) in
    let meth = Method (at_e, attr) in
    let at_meth =@ alloc (meth, Locked) in
    ret at_meth
  (* This is not handled yet, see class int (just above) *)
  | _ -> set_flag<addr> (Exn todo_error)
end


(** 6.5 Evaluation functions **)
val raise_exn<a> (exn : id) : m<a> =
  let at_exn =@ read_env exn in
  raise<a> at_exn

val eval_stmts (ss: body) : m<()> =
  mapM<stmt, ()> (eval_stmt, ss) ;@
  return<()> ()

(* Evaluation rules *)
val eval_stmt (s: stmt) : m<()> =
  match s with
  | SExpr e ->
      eval_expr e ;@
      return<()> ()
  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.71 *)
  | SAssign (x, v) ->
    let addr =@ eval_expr v in
    write_env (x, addr)
  | SAttrAssign (e, attr, v) ->
    let at_e =@ eval_expr e in
    let (objn, objs) =@ read at_e in
    let at_v =@ eval_expr v in
    let objs' =@
      match objs with
      | Locked ->
         (* Not possible to write in locked objs *)
         raise_exn<obj_s> _AttributeError
      | Fields fields ->
         let new_fields = map_insert<addr> (attr, at_v, fields) in
         return<obj_s> (Fields new_fields)
      end in
    write (at_e, (objn, objs'))
  | SDel id -> delete_env id
  (* Return, raise the ret flag *)
  | SPass -> return<()> ()
  | SReturn e ->
    let addr =@ eval_expr e in
    set_flag<()> (Ret addr)

  | SRaise e ->
    let at_exn =@ eval_expr e in
    raise<()> at_exn
  | STry (tbody, (exn, texc)) ->
    let flag =/ eval_stmts tbody in
    match flag with
    | Exn raised ->
      let baseException =@ read_env _BaseException in
      let at_exn =@ eval_expr exn in
      let valid_exception =@ is_subclass_of (at_exn, baseException) in
      match valid_exception with
      | True ->
        let exn_match_handler =@ is_instance_of (raised, at_exn) in
        match exn_match_handler with
        | True -> eval_stmts texc
        | False -> raise<()> raised
        end
      | False -> raise_exn<()> _TypeError
      end
    | flag -> set_flag<()> flag
    end

  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.115 *)
  | SFunctionDef (name, args, body) ->
    let body_locals =
      let loc = locals body in
      let non_locals = nonlocals body in
      let global_vars = globals body in
      let tmp = remove<id>(eq<id>, non_locals, loc) in
      remove<id>(eq<id>, global_vars, tmp) in
    let scope_up =@ get_scope_up () in
    let f = Fun(name, args, body_locals, body, scope_up) in
    let addr =@ alloc (f, Locked) in
	  write_env (name, addr)
  | SClassDef (name, args, kwargs, body) ->
    let dict =@ declarations' body in
    (* superclasses are in [args] *)
    let at_args =@ mapM<expr, addr> (eval_expr, args) in
    let c = Class(name, at_args, type_id, dict) in
    let addr =@ alloc (c, Fields map_empty<addr>) in
    write_env (name, addr)
  (* nonlocal and global are not evaluated, since they are parsing time information *)
  | SNonlocal _ -> return<()> ()
  | SGlobal _ -> return<()> ()
  end

val eval_call ((at_c: addr), (args: list<expr>)): m<addr> =
  let (objn, _) =@ read at_c in
  match objn with
  | Fun(_, a, loc, body, scope_up) ->
     let at_args =@ mapM<expr, addr> (eval_expr, args) in
     eval_fun_call (at_args, a, loc, body, scope_up)
  | Class(_, _, _, dict) ->
     let at_args =@ mapM<expr, addr> (eval_expr, args) in
     eval_class_call (at_args, at_c, dict)
  | Method(self, fun) ->
    eval_call (fun, Cons<expr> (Addr self, args))
  | Internal internal ->
    let at_args =@ mapM<expr, addr> (eval_expr, args) in
    call_internal (internal, at_args)
  | _ -> raise_exn<addr> _TypeError
  end


val eval_expr (e: expr): m<addr> =
  match e with
  | InternalToken token ->
    alloc (Internal token, Locked)

  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.70 *)
  | Constant constant -> eval_constant constant
  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.71 *)
  | Name id -> read_env id
  | Addr a -> ret a
  | Attribute (e, attr) ->
     let at_e =@ eval_expr e in
     get_attribute (at_e, attr)
  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.121 *)
  | Call(e_c, args) ->
    (* Eval the function *)
    let at_c =@ eval_expr e_c in
    eval_call (at_c, args)

  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.131 *)
  | BinOp(binop, e1, e2) ->
     let at1 =@ eval_expr e1 in
     let method = op(binop) in
     let type1 =@ typeof at1 in
     let has_attr =@ has_attribute (type1, method) in
     match has_attr with
     | True ->
        let fun =@ get_attribute (at1, method) in
        eval_call (fun, Cons<expr> (e2, Nil<expr>))
     | False ->
        raise_exn<addr> _TypeError
     end
  end

val eval_constant (c:constant) : m<addr> =
  let (builtins = builtins, local_env = _) =@ ask in
  match c with
  (* https://rmonat.fr/data/pubs/2021/thesis_monat.pdf#figure.caption.70 *)
  | CBool True -> ret builtins.true
  | CBool False -> ret builtins.false
  | CNone -> ret builtins.none
  | CInt n -> alloc (Int n, Locked)
  | CString s -> alloc (String s, Locked)
  end
