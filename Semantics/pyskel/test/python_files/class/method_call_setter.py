# __result__

class Foo:
    def bar(self):
        self.bar = 0


foo = Foo()
foo.bar()
__result__ = foo.bar
