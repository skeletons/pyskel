# __result__

class Foo:
    x = 2
    def f(self):
        return 40


foo = Foo()
__result__ = foo.f()
